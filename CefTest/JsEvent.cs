﻿using CefSharp;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CefTest
{
    public class JsEvent
    {
        public string messageText = "hi immessage";
        public string MyProperty { get; set; }

        public string showTest(string param)
        {
            FileInfo file = new FileInfo(@"D:\LkPower\admin\Anlaysis\00" + param + ".jpg");
            if (file.Exists) {
                file.Delete();
            }
            return "server";
        }
        /// <summary>
        /// 查找电脑信息
        /// </summary>
        /// <param name="javascriptCallback"></param>
        public void findComputerInfo(IJavascriptCallback javascriptCallback,string msg)
        {

            Task.Factory.StartNew(async () =>
            {

                using (javascriptCallback)
                {
                    Computer computer = new Computer();
                    string response = JsonConvert.SerializeObject(new
                    {
                        cpu_id = "I7",
                        disk_id = "XXGAGAX",
                        host_name = computer.HostName,
                        networkcard = "JJSUIJS",
                        serialNumber = computer.SerialNumber_Manufacturer_Product.Item1,
                        manufacturer = computer.SerialNumber_Manufacturer_Product.Item2,
                        product = computer.SerialNumber_Manufacturer_Product.Item3,
                    });
                    await javascriptCallback.ExecuteAsync(response);
                }
            });

        }
    }
}
