﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CefTest
{
    /// <summary>
    /// js c#回调类
    /// </summary>
    public class ScriptCallbackManager
    {
        /// <summary>
        /// 查找电脑信息
        /// </summary>
        /// <param name="javascriptCallback"></param>
        public void FindComputerInfo(IJavascriptCallback javascriptCallback)
        {

            Task.Factory.StartNew(async () =>
            {

                using (javascriptCallback)
                {
                    string response = "{\"key\":\"hello world!\"}";
                    await javascriptCallback.ExecuteAsync(response);
                }
            });

        }


    }
    /// <summary>
    /// 电脑信息类
    /// </summary>
    public class Computer
    {
        /// <summary>
        /// 获取序列号，制造商，型号
        /// </summary>
        public Tuple<string, string, string> SerialNumber_Manufacturer_Product
        {
            get
            {
                try
                {
                    Tuple<string, string, string> tuple = null; new Tuple<string, string, string>(string.Empty,
string.Empty, string.Empty);
     
    
                        tuple = new Tuple<string, string, string>("a","b","c");
                    return tuple;
                }
                catch (Exception)
                {

                    return null;
                }
            }
        }
        /// <summary>
        /// 计算机名称
        /// </summary>
        public string HostName
        {
            get
            {
                return System.Net.Dns.GetHostName();
            }
        }
    }
}
