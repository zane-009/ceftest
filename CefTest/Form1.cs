﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
namespace CefTest
{
    public partial class lkpower : Form
    {

        public lkpower()
        {
            InitializeComponent();
            InitBrowser();
            
        }
        public ChromiumWebBrowser browser;
        public void InitBrowser()
        {
            try
            {


                //lib = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"resources\cefsharp_x86\libcef.dll")
                var settings = new CefSettings();
                // Increase the log severity so CEF outputs detailed information, useful for debugging
                settings.LogSeverity = LogSeverity.Verbose;
                Cef.Initialize(settings);//"http://chrome.360.cn/test/core/"
                // By default CEF uses an in memory cache, to save cached data e.g. passwords you need to specify a cache path
                // NOTE: The executing user must have sufficent privileges to write to this folder.
                settings.CachePath = "cache";
                // Enable WebRTC                           
                //settings.CefCommandLineArgs.Add("enable-media-stream", "1");

                ChromiumWebBrowser br1 = new ChromiumWebBrowser("file:///E:/WorkRepertory/AIMicroProject/CefTest/CefTest/webapp/h1.html");
                br1.Width = 250;
                br1.Height = 300;
                br1.Dock = DockStyle.Right;
                this.Controls.Add(br1);


                browser = new ChromiumWebBrowser(@"E:\WorkRepertory\AIMicroProject\CefTest\CefTest\webapp\test.html");
                browser.Height = 300;
                browser.Width = 300;
                browser.Dock = DockStyle.Bottom;
                this.Controls.Add(browser);
                //browser.RegisterJsObject("bound", new lkpower());

                CefSharpSettings.LegacyJavascriptBindingEnabled = true;
                var jsEvent = new JsEvent();
                browser.RegisterAsyncJsObject("jsObj", jsEvent, BindingOptions.DefaultBinder);

                //注意在js函数里面只驼峰写法开头，如果要使用C#写法就使用下面的注册方式。
                //BindingOptions bo = new BindingOptions();
                //bo.CamelCaseJavascriptNames = false;
                //browser.RegisterJsObject("usbKey", new UsbKeyBound(), bo);
                
                //browser.RegisterJsObject("jsObj", new JsEvent());
                //browser.RegisterAsyncJsObject("TestJsObject", _thisJsObject, false);

            }
            catch (Exception ex) {
                throw ex;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //browser.ExecuteScriptAsync("show('aabbcc')");
            browser.ExecuteScriptAsync("show('aabbcc')");
        }
        private void serverMethod(string s) {
            MessageBox.Show(s);
        }

        private void lkpower_FormClosed(object sender, FormClosedEventArgs e)
        {
            Cef.Shutdown();
        }

        private void btn_test_Click(object sender, EventArgs e)
        {
            Task<CefSharp.JavascriptResponse> t = browser.EvaluateScriptAsync("show('aabbcc')");
            // 等待js 方法执行完后，获取返回值

            t.Wait();
            // t.Result 是 CefSharp.JavascriptResponse 对象
            // t.Result.Result 是一个 object 对象，来自js的 callTest2() 方法的返回值
            if (t.Result.Result != null)
            {
                MessageBox.Show(t.Result.Result.ToString());
            }
        }
    }
}
