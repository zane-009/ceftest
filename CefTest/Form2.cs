﻿using CefSharp;
using CefSharp.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CefTest
{
    public partial class Form2 : Form
    {
        public ChromiumWebBrowser browser;
        public Form2()
        {
            InitializeComponent();
            InitBrowser();
        }
        public void InitBrowser()
        {
            try
            {


                var settings = new CefSettings();
                settings.LogSeverity = LogSeverity.Verbose;
                Cef.Initialize(settings);
                // By default CEF uses an in memory cache, to save cached data e.g. passwords you need to specify a cache path
                // NOTE: The executing user must have sufficent privileges to write to this folder.
                settings.CachePath = "cache";
                ChromiumWebBrowser browser = new ChromiumWebBrowser("file:///E:/WorkRepertory/AIMicroProject/CefTest/CefTest/webapp/cameraTaggingForm.html");//analysisResult.html"); 
                browser.Dock = DockStyle.Fill;
                this.Controls.Add(browser);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
